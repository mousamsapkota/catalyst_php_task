<?php
class CsvToDatabase
{
    private $file_path;
    private $host;
    private $user;
    private $password;
    private $database;
    private $mysqli;

    public function __construct($file_path, $host, $user, $password, $database)
    {
        $this->file_path = $file_path;
        $this->host = $host;
        $this->user = $user;
        $this->password = $password;
        $this->database = $database;

        $this->mysqli = new mysqli($this->host, $this->user, $this->password, $this->database);
        if ($this->mysqli->connect_error) {
            die('Connect Error (' . $this->mysqli->connect_errno . ') ' . $this->mysqli->connect_error);
        }
    }

    public function createTable()
    {
        $sql_create_table = "
            CREATE TABLE IF NOT EXISTS `users` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `name` varchar(255) NOT NULL,
              `surname` varchar(255) NOT NULL,
              `email` varchar(255) NOT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
        ";

        if (!$this->mysqli->query($sql_create_table)) {
            echo "Error creating table: " . $this->mysqli->error;
            exit();
        }
    }
    public function checkUsers()
    {
        $sql_check = "SELECT table_name FROM information_schema.tables WHERE TABLE_SCHEMA = '{$this->database}'";

        $result = $this->mysqli->query($sql_check);

        if ($result->num_rows > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public function insertRecords()
    {
        $file_handle = fopen($this->file_path, 'r');

        $index = 0;
        while (($row = fgetcsv($file_handle)) !== false) {
            if ($index > 0) {
                $name = ucwords(strtolower($row[0]));
                $surname = ucwords(strtolower(str_replace("'", "''", $row[1])));
                $email = strtolower(str_replace("'", "''", $row[2]));

                if (!(filter_var($email, FILTER_VALIDATE_EMAIL))) {
                    echo "Error: Invalid email format - {$email}\n";
                    continue;
                }

                $sql_insert = "
                    INSERT INTO `users` (`name`, `surname`, `email`)
                    VALUES ('{$name}', '{$surname}', '{$email}')
                ";

                if (!$this->mysqli->query($sql_insert)) {
                    echo "Error inserting record: " . $this->mysqli->error;
                    continue;
                }
            }
            $index++;
        }

        fclose($file_handle);
    }

    public function closeConnection()
    {
        $this->mysqli->close();
    }
}

$options = getopt("", ["file:","u:","p:","create_table:","help:","h:","dry_run:"]);
//check options
if (!empty($options['file'])) {
    echo $options['file'];
    if (empty($options['u'])) {
        exit();
    }
}

if (!empty($options['u'])) {
    echo $options['u'];
    if (empty($options['p'])) {
        exit();
    }
}


if (!empty($options['p'])) {
    echo $options['p'];
    if (empty($options['u'])) {
        exit();
    }
}

if (!empty($options['h'])) {
    echo $options['h'];
    if (empty($options['u'])) {
        exit();
    }
}



if (!empty($options['file']) && !empty($options['u']) && !empty($options['p']) && !empty($options['h'])) {

    $file_path = $options['file'];
    $host = $options['h'];
    $user = $options['u'];
    $password = $options['p']==''?'':'';
}
else
{
    //default
    $file_path = 'C:\Users\61450\Downloads\users.csv';
    $host = 'localhost';
    $user = 'catalyst_user';
    $password = 'catalyst_pwd_0@';
}
$database = 'catalyst';

$obj = new CsvToDatabase($file_path, $host, $user, $password, $database);

if (!empty($options['create_table'])) {
    //check if already table exists
    if($obj->checkUsers()==1)
    {
        echo "Table already exists";
        exit();
    }
    $obj->createTable();
    echo "Table Created Successfully";
    exit();
}
if (!empty($options['dry_run']) && !empty($options['file'])) {
    echo $options['file']."is executed Successfully without altering table";
    exit();
}


$obj->insertRecords();
$obj->closeConnection();
